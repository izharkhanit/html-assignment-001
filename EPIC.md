# Scrum style #

## Epic: ##

### As a Product Owner ###
### I want to create a web app to handle tasks ###
### So we can provide this service to our current customers ###

* The customer should be able to create/add tasks.
* The customer should be able to edit a task
* The customer should be able to create/add categories.
* The customer should be able to edit a category.
* The customer should be able to group their tasks by category.
* The customer should be able to toggle a task  to done or not done.
* The customer should be able to add the deadline date.
* The customer should be able to clear all the tasks in done status.
* The customer should be able to filter the tasks by description.

------

## User Story: ##

### As an user ###
### I want to add new tasks to my todo app ###
### So I can track them in the app ###

* Use a modal to show the ‘Add Task’ form. (see designs).
* The user should be able to set a description and a deadline date for the task (see designs).
* The new task should be added to the tasks section (see designs).
* When the task is set to done it should be shown strike-through (see designs).
* Each task item in the task list should have a checkbox to toggle the state of the task.
* The deadline date/icon should have three different status in the task list:
    * Green: The deadline is longer than 7 days since today.
    * Orange: The deadline is shorter than 7 days but longer than 2 days.
    * Red: The deadline is shorter than 2 days.

------

## User Story: ##

### As an user ###
### I want to edit existing tasks ###
### So I can modify the description and/or the deadline date ###

* Use a modal to show the ‘Edit Task’ form. (see designs)
* The changes will be applied to the view immediately.
* The user should be able to modify the description and/or the deadline date.
* The user should not be able to change the state of the task using this method.

------

## User Story: ##

### As an user ##
### I want to add new categories to my todo app ###
### So I can organize my tasks in the app ###

* Use a modal to show the ‘Add Category’ form. (see designs)
* The user should be able to set a Category name.
* The new category should be added to the categories section (see designs)
* The category should be clickable, and when a category is clicked:
    * It should be highlighted.
    * The content section should:
        * Change the title from the previous category name to the name of the selected category.
        * Change the filter and show only the tasks that belong to the selected category.
        * On adding a new task with a category selected the new task should be assigned to the selected category.
* A ‘All tasks’ category name must exist by default and it will show all the tasks of all the categories.

------

## User Story: ##

### As an user ##
### I want to edit existing categories ###
### So I can modify the category name ###

* Use a modal to show the ‘Edit Category’ form. (see designs)
* The changes will be applied to the view immediately.
* The user should be able to modify the Category name.
* All the tasks belonging to the category should still belong to that category.

------

## User Story: ##

### As an user ##
### I want to filter my tasks by description ###
### So I can check related tasks at a glance ###

* Use a search input to filter by task description. (see designs)
* The changes will be applied to the view immediately.

------

## User Story: ##

### As an user ##
### I want to clear all my fulfilled tasks ###
### So I can better track the existing task ###

* Use a button to remove all the fulfilled tasks in the filtered view (see designs)
* Regarding the selected category on pressing the button it should remove all the tasks with status “done” 
* The changes will be applied to the view immediately


------