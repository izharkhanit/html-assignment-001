"use strict";
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
// var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var spanone = document.getElementById("closeone");
var flag = 0;

// When the user clicks the button, open the modal 
// btn.addEventListener("click", myFunction);
spanone.addEventListener("click",myCloseFunction);

// function myFunction()
// {
//     modal.style.display = "block";
// }

function addtaskbtnclicked(val){
    flag = val;
    if(flag==2){
         document.getElementById("category").style.display = "none";
         document.getElementById("categoryname").style.display = "none";
         document.getElementById("taskclicked").style.display = "block";
         document.getElementById("description").style.display = "block";
         document.getElementById("datedeadline").style.display = "block";
    }
    modal.style.display = "block";
  }

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

function categorybtnclicked(val){
    flag = val;
    if(flag==1){
         document.getElementById("taskclicked").style.display = "none";
         document.getElementById("description").style.display = "none";
         document.getElementById("datedeadline").style.display = "none";
         document.getElementById("category").style.display = "block";
         document.getElementById("categoryname").style.display = "block";
    }
    modal.style.display = "block";
  }

  function saveTask() {
    var tr = document.createElement("tr");
    var inputValue = document.getElementById("taskId").value;
    var inputValuecategory = document.getElementById("categoryid").value;
    var datetime = document.getElementById("deadlineDate").value;
    var chk = "<tr><td class='less_width'> <input type='checkbox' class='strikeoutcliked' ></td><td class='extra_width'><h5>"+inputValue+"<span class='glyphicon glyphicon-time marginleft'><small>"+datetime+"</small></span> </h5></td></tr>";
   var categorytxt = "<span id='icontxt' class='glyphicon glyphicon-th-list'>  "+inputValuecategory+"</span><br>";
    if (inputValue === '') {
        if(inputValuecategory === ''){
            alert("You Must enter some category!");
        } else {
            if(inputValuecategory != ''){
                alert(inputValuecategory);
                $(".marginleftfive").append(categorytxt);
            }
           
        }
      } else {
        $("#mytable").append(chk); 
        modal.style.display = "none";
      }
}

function filterTable() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("filter");
    filter = input.value.toUpperCase();
    table = document.getElementById("mytable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];

        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function removeAll(){
    var Parent = document.getElementById("mytable");
    while(Parent.hasChildNodes())
    {
       Parent.removeChild(Parent.firstChild);
    }
}

$('.strikeoutcliked').change(function() {
    if ( this.checked) {
      $(this).parent().parent().addClass("strikeout");
    } else {
      $(this).parent().parent().removeClass("strikeout");
    }
 });

function myCloseFunction()
{
    modal.style.display = "none";
}

//When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}